import { Component, OnInit } from '@angular/core';
import { Chance } from 'chance';
import * as Highcharts from 'highcharts';
import * as HighchartsStock from 'highcharts/highstock';

@Component({
	selector: 'app-stocks-prototype',
	templateUrl: './stocks-prototype.component.html',
	styleUrls: ['./stocks-prototype.component.sass']
})
export class StocksPrototypeComponent implements OnInit {
	HighchartsStock: typeof Highcharts = HighchartsStock;

	constructorType = 'stockChart';
	chart: Highcharts.Options;

	chance;

	constructor() {
		this.chance = new Chance();
	}

	ngOnInit() {
		let i = 0;
		const date = new Date().getTime();

		this.chart = {
			title: {
				text: ''
			},
			series: [{
				type: 'line',
				data: Array.from({ length: 10000 }, () => [date + (600000 * i++), this.chance.floating({ min: 0, max: 100 })]),
			}]
		};
	}
}
