import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StocksPrototypeComponent } from './stocks-prototype.component';

const routes: Routes = [
    {
        path: '',
        component: StocksPrototypeComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: []
})
export class StocksPrototypeRoutingModule { }
