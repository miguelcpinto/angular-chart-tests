import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HighchartsChartModule } from 'highcharts-angular';
import { StocksPrototypeComponent } from './stocks-prototype.component';
import { StocksPrototypeRoutingModule } from './stocks-prototype-routing.module';

@NgModule({
    imports: [
        CommonModule,
        HighchartsChartModule,
        StocksPrototypeRoutingModule
    ],
    declarations: [StocksPrototypeComponent]
})
export class StocksPrototypeModule { }
