import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StocksPrototypeComponent } from './stocks-prototype.component';

describe('StocksPrototypeComponent', () => {
  let component: StocksPrototypeComponent;
  let fixture: ComponentFixture<StocksPrototypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StocksPrototypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StocksPrototypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
