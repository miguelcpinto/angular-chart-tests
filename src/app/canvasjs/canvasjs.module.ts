import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CanvasJSComponent } from './canvasjs.component';
import { CanvasJSRoutingModule } from './canvasjs-routing.module';

@NgModule({
	imports: [
		CommonModule,
		CanvasJSRoutingModule
	],
	declarations: [CanvasJSComponent]
})
export class CanvasJSModule { }
