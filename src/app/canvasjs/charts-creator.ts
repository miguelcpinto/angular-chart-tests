import * as CanvasJS from '@assets/canvasjs.min.js';

function createDataPointsCharts(Chart) {
	const chart = new Chart('dataPointsContainer', {
		zoomEnabled: true,
		zoomType: 'xy',
  animationEnabled: true,
  axisX: {
	stripLines: [
	{
		startValue: 100,
		endValue: 200,
		color: '#d8d8d8'
  },
	{
		startValue: 300,
		endValue: 350,
		color: '#ffff00'
  }

	],
	valueFormatString: '####'
  },
	axisY: [ {
		title: 'Speed',
		titleFontColor: '#4F81BC',
		suffix : ' m/s',
		lineColor: '#4F81BC',
		tickColor: '#4F81BC'
  },
  {
		title: 'Another distance',
		lineColor: '#369EAD',
		tickColor: '#369EAD',
		labelFontColor: '#369EAD',
		titleFontColor: '#369EAD',
		suffix: 'k'
	}

],
	axisY2: {
		title: 'Distance',
		titleFontColor: '#C0504E',
		suffix : ' m',
		lineColor: '#C0504E',
		tickColor: '#C0504E'
	},
	exportEnabled: true,
	rangeChanged: (e) => {
		console.log(`
			type : ${e.type},
			trigger : ${e.trigger},
			AxisX viewportMininum : ${e.axisX[0].viewportMinimum},
			AxisX viewportMaximum : ${e.axisX[0].viewportMaximum}
		`);
	},
	rangeChanging: (e) => {
		console.log(`
			type : ${e.type},
			trigger : ${e.trigger},
			AxisX viewportMininum : ${e.axisX[0].viewportMinimum},
			AxisX viewportMaximum : ${e.axisX[0].viewportMaximum}
		`);
	},
		title: {
			text: 'Gaps Between Time Points'
		},
		subtitles: [{
			text: 'Try Zooming and Panning'
		}],
		data: []
	});

 return chart;
}

function createMixChart() {
  const chart = new CanvasJS.Chart('mixContainer', {
  animationEnabled: true,
  zoomEnabled: true,
  zoomType: 'xy',
	theme: 'light2',
	title: {
		text: 'Mix Charts'
	},
	axisX: {
		valueFormatString: 'MMM'
	},
	axisY: {
		prefix: '$',
		labelFormatter: addSymbols
	},
	toolTip: {
		shared: true
	},
	legend: {
		cursor: 'pointer',
		itemclick: toggleDataSeries
	},
	data: [
	{
		type: 'column',
		name: 'Actual Sales',
		showInLegend: true,
		xValueFormatString: 'MMMM YYYY',
		yValueFormatString: '$#,##0',
		dataPoints: [
		{ x: new Date(2016, 0), y: 20000 },
		{ x: new Date(2016, 1), y: 30000 },
		{ x: new Date(2016, 2), y: 25000 },
		{ x: new Date(2016, 3), y: 70000, indexLabel: 'High Renewals' },
		{ x: new Date(2016, 4), y: 50000 },
		{ x: new Date(2016, 5), y: 35000 },
		{ x: new Date(2016, 6), y: 30000 },
		{ x: new Date(2016, 7), y: 43000 },
		{ x: new Date(2016, 8), y: 35000 },
		{ x: new Date(2016, 9), y:  30000},
		{ x: new Date(2016, 10), y: 40000 },
		{ x: new Date(2016, 11), y: 50000 }
		]
	},
	{
		type: 'line',
		name: 'Expected Sales',
		showInLegend: true,
		yValueFormatString: '$#,##0',
		dataPoints: [
		{ x: new Date(2016, 0), y: 40000 },
		{ x: new Date(2016, 1), y: 42000 },
		{ x: new Date(2016, 2), y: 45000 },
		{ x: new Date(2016, 3), y: 45000 },
		{ x: new Date(2016, 4), y: 47000 },
		{ x: new Date(2016, 5), y: 43000 },
		{ x: new Date(2016, 6), y: 42000 },
		{ x: new Date(2016, 7), y: 43000 },
		{ x: new Date(2016, 8), y: 41000 },
		{ x: new Date(2016, 9), y: 45000 },
		{ x: new Date(2016, 10), y: 42000 },
		{ x: new Date(2016, 11), y: 50000 }
		]
	},
	{
		type: 'area',
		name: 'Profit',
		markerBorderColor: 'white',
		markerBorderThickness: 2,
		showInLegend: true,
		yValueFormatString: '$#,##0',
		dataPoints: [
		{ x: new Date(2016, 0), y: 5000 },
		{ x: new Date(2016, 1), y: 7000 },
		{ x: new Date(2016, 2), y: 6000},
		{ x: new Date(2016, 3), y: 30000 },
		{ x: new Date(2016, 4), y: 20000 },
		{ x: new Date(2016, 5), y: 15000 },
		{ x: new Date(2016, 6), y: 13000 },
		{ x: new Date(2016, 7), y: 20000 },
		{ x: new Date(2016, 8), y: 15000 },
		{ x: new Date(2016, 9), y:  10000},
		{ x: new Date(2016, 10), y: 19000 },
		{ x: new Date(2016, 11), y: 22000 }
		]
	}]
  });
  chart.render();

  function addSymbols(e) {
	const suffixes = ['', 'K', 'M', 'B'];
	let order = Math.max(Math.floor(Math.log(e.value) / Math.log(1000)), 0);

	if (order > suffixes.length - 1) {
		order = suffixes.length - 1;
	}

	const suffix = suffixes[order];
	return CanvasJS.formatNumber(e.value / Math.pow(1000, order)) + suffix;
  }

  function toggleDataSeries(e) {
	if (typeof (e.dataSeries.visible) === 'undefined' || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	} else {
		e.dataSeries.visible = true;
	}
	e.chart.render();
  }

}

function createCrossAirChart() {
const chart = new CanvasJS.Chart('crossContainer', {
	animationEnabled: true,
	theme: 'light2',
	title: {
		text: 'Site Traffic'
	},
	axisX: {
		valueFormatString: 'DD MMM',
		crosshair: {
			enabled: true,
			snapToDataPoint: true
		}
	},
	axisY: {
		title: 'Number of Visits',
		crosshair: {
			enabled: true
		}
	},
	toolTip: {
		shared: true
	},
	legend: {
		cursor: 'pointer',
		verticalAlign: 'bottom',
		horizontalAlign: 'left',
		dockInsidePlotArea: true,
		itemclick: toogleDataSeries
	},
	data: [{
		type: 'line',
		showInLegend: true,
		name: 'Total Visit',
		markerType: 'square',
		xValueFormatString: 'DD MMM, YYYY',
		color: '#F08080',
		dataPoints: [
			{ x: new Date(2017, 0, 3), y: 650 },
			{ x: new Date(2017, 0, 4), y: 700 },
			{ x: new Date(2017, 0, 5), y: 710 },
			{ x: new Date(2017, 0, 6), y: 658 },
			{ x: new Date(2017, 0, 7), y: 734 },
			{ x: new Date(2017, 0, 8), y: 963 },
			{ x: new Date(2017, 0, 9), y: 847 },
			{ x: new Date(2017, 0, 10), y: 853 },
			{ x: new Date(2017, 0, 11), y: 869 },
			{ x: new Date(2017, 0, 12), y: 943 },
			{ x: new Date(2017, 0, 13), y: 970 },
			{ x: new Date(2017, 0, 14), y: 869 },
			{ x: new Date(2017, 0, 15), y: 890 },
			{ x: new Date(2017, 0, 16), y: 930 }
		]
	},
	{
		type: 'line',
		showInLegend: true,
		name: 'Unique Visit',
		lineDashType: 'dash',
		dataPoints: [
			{ x: new Date(2017, 0, 3), y: 510 },
			{ x: new Date(2017, 0, 4), y: 560 },
			{ x: new Date(2017, 0, 5), y: 540 },
			{ x: new Date(2017, 0, 6), y: 558 },
			{ x: new Date(2017, 0, 7), y: 544 },
			{ x: new Date(2017, 0, 8), y: 693 },
			{ x: new Date(2017, 0, 9), y: 657 },
			{ x: new Date(2017, 0, 10), y: 663 },
			{ x: new Date(2017, 0, 11), y: 639 },
			{ x: new Date(2017, 0, 12), y: 673 },
			{ x: new Date(2017, 0, 13), y: 660 },
			{ x: new Date(2017, 0, 14), y: 562 },
			{ x: new Date(2017, 0, 15), y: 643 },
			{ x: new Date(2017, 0, 16), y: 570 }
		]
	}]
});
chart.render();

function toogleDataSeries(e) {
	if (typeof(e.dataSeries.visible) === 'undefined' || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	} else {
		e.dataSeries.visible = true;
	}
	chart.render();
}

}

export default {
  createMixChart,
  createDataPointsCharts,
  createCrossAirChart
};
