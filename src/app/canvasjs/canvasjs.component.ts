import { Component, OnInit, OnDestroy } from '@angular/core';
import * as CanvasJS from '@assets/canvasjs.min.js';
import { HttpClient } from '@angular/common/http';
import { webSocket } from 'rxjs/webSocket';
import { Subscription } from 'rxjs';
import charts from './charts-creator';
import Canvas from 'canvasjs';

@Component({
	selector: 'app-canvasjs',
	templateUrl: './canvasjs.component.html',
	styleUrls: ['./canvasjs.component.css']
})
export class CanvasJSComponent implements OnInit, OnDestroy {
	private socket$: Subscription;

	constructor(private http: HttpClient) {
	CanvasJS.addColorSet(
		'customColorSet1',
		[ '#de350b' ]);
  }

	ngOnInit() {
		this.createDataPointsChart();
		charts.createMixChart();
	 this.createLiveChart();
	 charts.createCrossAirChart();
	}

	createDataPointsChart() {
		const chart = charts.createDataPointsCharts(CanvasJS.Chart) as Canvas.Chart;

		chart.options.data.push({
			type: 'line',
			dataPoints: []
		}, {
			type: 'line',

			dataPoints: []
		}, {
			type: 'line',
			dataPoints: []
		}, {
		  type: 'line',
		  toolTipContent: '<br/> <div class="title">Custom One:</div> {y} ',
			dataPoints: []
		}, {
		type: 'spline',
		axisYType: 'secondary',
		name: 'distance covered',
		yValueFormatString: '#,##0.# m',
		dataPoints: [
			{ x: 0 , y: 0 },
			{ x: 11 , y: 90 },
			{ x: 47 , y: 1590 },
			{ x: 56 , y: 1740 },
			{ x: 120 , y: 3740 },
			{ x: 131 , y: 3940 },
			{ x: 171 , y: 5190 },
			{ x: 189 , y: 6290 },
			{ x: 221 , y: 7590 },
			{ x: 232 , y: 7790 },
			{ x: 249 , y: 8390 },
			{ x: 253 , y: 8440 },
			{ x: 264 , y: 8620 },
			{ x: 280 , y: 9220 },
			{ x: 303 , y: 9780 },
			{ x: 346 , y: 10780 },
			{ x: 376 , y: 11120 },
			{ x: 388 , y: 11220 },
			{ x: 430 , y: 11300 },
			{ x: 451 , y: 11400 }
		]
  },
  {
		type: 'line',
		name: 'Another distance',
		color: '#369EAD',
		showInLegend: true,
		axisYIndex: 1,
		dataPoints: [
		{ x: 0 - 20 , y: 0 },
			{ x: 11 - 20 , y: 90 },
			{ x: 47 - 20 , y: 1590 },
			{ x: 56 - 20 , y: 1740 },
			{ x: 120 - 20 , y: 3740 },
			{ x: 131 - 20 , y: 3940 },
			{ x: 171 - 20 , y: 5190 },
			{ x: 189 - 20 , y: 6290 },
			{ x: 221 - 20 , y: 7590 },
			{ x: 232 - 20 , y: 7790 },
			{ x: 249 - 20 , y: 8390 },
			{ x: 253 - 20 , y: 8440 },
			{ x: 264 - 20 , y: 8620 },
			{ x: 280 - 20 , y: 9220 },
			{ x: 303 - 20 , y: 9780 },
			{ x: 346 - 20 , y: 10780 },
			{ x: 376 - 20 , y: 11120 },
			{ x: 388 - 20 , y: 11220 },
			{ x: 430 - 20 , y: 11300 },
			{ x: 451 - 20 , y: 11400 }
		]
	}, );

		const basePointConfig: Canvas.ChartDataPoint = {
			color: '#de350b',
			indexLabelLineColor: '#de350b',
			markerType: 'none',
			click: (...args) => console.log(...args)
		};

		for (let i = 0; i < 10; i++) {
			chart.options.data[0].dataPoints.push({ y: i, x: i, color: '#de350b', click: (...args) => console.log('Arguments', ...args) });
			chart.options.data[1].dataPoints.push({ y: i + 20, x: i + 20, ...basePointConfig });
			chart.options.data[2].dataPoints.push({ y: i + 40, x: i + 40, color: '#de350b', click: (...args) => console.log('Arguments', ...args) });
			chart.options.data[3].dataPoints.push({ y: i + 60, x: i + 60, ...basePointConfig });
  }

	 chart.options.colorSet = 'colorSet2';

		chart.render();
	}

	createLiveChart() {
		const dataPoints = [];
		const chart = new CanvasJS.Chart('liveChartContainer', {
			exportEnabled: true,
			animationEnabled: true,
			title: {
				text: 'Live Chart with Data-Points from External JSON'
			},
			data: [{
				type: 'spline',
				dataPoints
			}]
		});

		let count = 1;

		this.socket$ = webSocket('ws://localhost:8999').subscribe((value: number) => {

		dataPoints.length % 10 === 0 ? dataPoints.push({ x: count, y: null }) : dataPoints.push({ x: count, y: value });

		count++;

		if (dataPoints.length > 100) {
			dataPoints.shift();
		}
		chart.render();
		});
	}

	ngOnDestroy() {
		this.socket$.unsubscribe();
	}
}
