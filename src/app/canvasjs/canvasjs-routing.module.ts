import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanvasJSComponent } from './canvasjs.component';

const routes: Routes = [
    {
        path: '',
        component: CanvasJSComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: []
})
export class CanvasJSRoutingModule { }
