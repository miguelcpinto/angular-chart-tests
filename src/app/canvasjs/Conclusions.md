## CanvasJS

### Checks

YES  -- Zoom on x axis   
  Easy interval zoom and easy zoom undo.

YES  -- Zoom on y axis   

YES  -- Panning   
  It's a mode which we need to enable.

YES  -- Animations in Static Charts
 NO  -- Animations in Live Charts

 Only animations when drawing static charts, if we add one point it will not animate anything.
 The animation is not customisable, only one animation, that we can enable/disable.

SO SO...  -- acceptable customisation of an overlay tooltip;    
    https://canvasjs.com/docs/charts/basics-of-creating-html5-chart/tool-tip/    
    https://jsfiddle.net/canvasjs/qmwjrx28/  

  html in string format that can have binding.   
  It's not possible to use a angular component.   
  The css can be overwritten but it is hard and most of the properties need to have a !important.   


YES  -- draw gaps between time points;   ****

YES  -- highlight points on hover   

YES  -- highlight data drawn on hover of the assigned Y axis   

YES  -- click on a point event and the mouse coordinates to show a popup to add a (data note)   
    DOUBT: This is just a callback, right?


YES  -- highlight points from an external source/component   
    This works by changing the points in the interval we want.
    mouse interaction and 
    https://jsfiddle.net/canvasjs/28uo3yvt/


YES -- draw points and lines on the same chart.   


YES  -- draw points from a socket   
    But, with no animation.


YES   -- select multiple zones to Time comparison overlay   
    https://jsfiddle.net/canvasjs/28uo3yvt/
    https://canvasjs.com/docs/charts/chart-options/axisx/strip-trend-lines/

YES  -- highlight several zones to show stages   


YES  -- multiple y axis   
    https://canvasjs.com/javascript-charts/line-chart-multiple-axis/



#### Advantages
- Everything in the chart is customisable.
- Has all the features needed.
- Very good Performance in every device.
- Size (minified without tree shaking) - 459 KB
- Documentation

#### Disavantages
- Visually very ugly.
- Customisation is a bit costly.
- Animations only on chart first render.
- Number of animations: 1 per chart - not custom.


#### Price
 - 2000$ a year
