import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StocksAxisComponent } from './stocks-axis.component';

const routes: Routes = [
    {
        path: '',
        component: StocksAxisComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: []
})
export class StocksAxisRoutingModule { }
