import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StocksAxisComponent } from './stocks-axis.component';

describe('StocksAxisComponent', () => {
  let component: StocksAxisComponent;
  let fixture: ComponentFixture<StocksAxisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StocksAxisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StocksAxisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
