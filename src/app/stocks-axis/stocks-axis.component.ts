import { Component, OnInit } from '@angular/core';
import { Chance } from 'chance';
import * as Highcharts from 'highcharts';
import * as HighchartsStock from 'highcharts/highstock';
import moment from 'moment';
import debounce from 'lodash';

enum Datalabels {
	circle,
	square,
	triangle,
	groupNumber
}

export const DATALABELS_CONFIG = {
	[Datalabels.circle]: 'ic-circle.svg',
	[Datalabels.square]: 'ic-square.svg',
	[Datalabels.triangle]: 'ic-triangule.svg',
	[Datalabels.groupNumber]: 'group.svg',
};

@Component({
	selector: 'app-stocks-axis',
	templateUrl: './stocks-axis.component.html',
	styleUrls: ['./stocks-axis.component.sass']
})
export class StocksAxisComponent implements OnInit {

	constructor() {
		this.chance = new Chance();
	}
	HighchartsStock: typeof Highcharts = HighchartsStock;

	constructorType = 'stockChart';
	chartInstance: Highcharts.Chart;
	chart: Highcharts.Options;

	chance;
	data: any[];

	debounced;

	serieData;

	ngOnInit() {
		const i = 0;
		const j = 0;
		const date1 = moment();
		const date2 = moment(date1);
		const date3 = moment(date1);
		const date4 = moment(date1);

		this.serieData = Array.from({ length: 10000 }, () => [date1.add(1, 'h').valueOf(), this.chance.floating({ min: 0, max: 1000 })]);

		// this.chart = {
		// 	title: {
		// 		text: ''
		// 	},
		// 	credits: {
		// 		enabled: false
		// 	},

		// 	yAxis: [
		// 		{
		// 			startOnTick: false,
		// 			endOnTick: false,
		// 			labels: {
		// 				enabled: false,
		// 			},
		// 			title: {
		// 				text: undefined
		// 			},
		// 			height: '20%',
		// 			lineWidth: 0,
		// 			resize: {
		// 				enabled: true
		// 			}
		// 		}, {
		// 			title: {
		// 				text: 'Volume'
		// 			},
		// 			top: '20%',
		// 			height: '80%',
		// 			offset: 0,
		// 			lineWidth: 2
		// 		}
		// 	],

		// 	tooltip: {
		// 		shared: false
		// 	},

		// 	series: [
		// 		{
		// 			yAxis: 0,
		// 			type: 'line',
		// 			data: Array.from({ length: 10000 }, () => {
		// 				const val = Math.round(Math.random());
		// 				return [date + (600000 * j++), 1];
		// 			}),

		// 			tooltip: {
		// 				pointFormatter: () => false as any
		// 			}
		// 		},
		// 		{
		// 			yAxis: 1,
		// 			type: 'line',
		// 			data: Array.from({ length: 10000 }, () => [date + (600000 * i++), this.chance.floating({ min: 0, max: 1000 })]),
		// 		}
		// 	]
		// };

		// this.data = Array.from({ length: 10000 }, () => [date1.add(1, 'h').valueOf(), this.chance.floating({ min: 0, max: 1000 })]);
		const newData = this.serieData.slice(2000, 6000);

		this.chart = {
			rangeSelector: {
				enabled: false
			},

			navigator: {
				adaptToUpdatedData: false,
				series: {
					data: this.serieData
				}
			},
			xAxis: {
				events: {
					setExtremes: this.setDataExtremes.bind(this)
				}
			},

			title: {
				text: ''
			},
			credits: {
				enabled: false
			},
			chart: {
				zoomType: 'x',
				// panKey: 'shift',
				// panning: {
				// 	enabled: true
				// }
			},



			yAxis: [
				{
					className: 'pato',
					top: '15%',
					height: '85%',
				}, {
					labels: {
						enabled: false
					},
					startOnTick: false,
					endOnTick: false,
					lineWidth: 0,
					height: '15%',
					resize: {
						enabled: true
					}
				}
			],

			tooltip: {
				shared: true,
				// split: true
			},

			plotOptions: {
				series: {
					states: {
						inactive: {
							opacity: 1
						}
					}
				},
				flags: {
					cursor: 'pointer',
					onSeries: 'datalabels',
					tooltip: {
						pointFormatter: () => null
					},
					events: {
						click: (ev) => {
							console.log('click', ev, this);
							this.chartInstance.xAxis[0].addPlotLine({
								value: ev.point.x,
								color: '#333',
								width: 1.5,
								zIndex: 2,
							});
						}
					}
				}
			},

			series: [
				{
					yAxis: 0,
					type: 'line',
					data: newData,
					zIndex: 4
				},
				// {
				// 	yAxis: 0,
				// 	type: 'line',
				// 	data: Array.from({ length: 10000 }, () => [date4.add(1, 'h').valueOf(), this.chance.floating({ min: 100, max: 500 })]),
				// 	zIndex: 4
				// },
				{
					id: 'datalabels',
					yAxis: 1,
					type: 'line',
					data: newData.map(([time, _]) => ([time, 1])),
					color: '#333',
					enableMouseTracking: false,
				},
				this.generateFlags(this.serieData, Datalabels.circle),
				// this.generateFlags(this.serieData, Datalabels.triangle),
				this.generateFlags(this.serieData, Datalabels.square),
				this.generateFlags(this.serieData, Datalabels.groupNumber),
			]
		};
	}

	generateFlags(serieData, dataType): any {
		const data = Array.from({ length: 4 }, () => this.chance.integer({ min: 0, max: 10000 })).sort((x, y) => x - y).map(i => ({
			x: serieData[i][0],
			title: dataType === Datalabels.groupNumber ? 10 : ' '
		}));

		return {
			type: 'flags',
			data,
			shape: `url(assets/${DATALABELS_CONFIG[dataType]})` as any,
			y: dataType === Datalabels.groupNumber ? -10 : -4,
			zIndex: 3
		};
	}

	setDataExtremes(event: Highcharts.AxisSetExtremesEventObject) {
		event.preventDefault();

		// @ts-ignore
		const minIndex = this.serieData.findIndex(elem => elem[0] >= event.min);
		// @ts-ignore
		const maxIndex = this.serieData.findIndex(elem => elem[0] >= event.max);

		this.chartInstance.series[0].setData(this.serieData.slice(minIndex, maxIndex));
		this.chartInstance.series[1].setData(this.serieData.slice(minIndex, maxIndex).map(([time, _]) => ([time, 1])));
	}
}
