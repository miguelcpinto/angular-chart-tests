import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HighchartsChartModule } from 'highcharts-angular';
import { StocksAxisComponent } from './stocks-axis.component';
import { StocksAxisRoutingModule } from './stocks-axis-routing.module';

@NgModule({
    imports: [
        CommonModule,
        HighchartsChartModule,
        StocksAxisRoutingModule
    ],
    declarations: [StocksAxisComponent]
})
export class StocksAxisModule { }
