import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
	{
		path: 'highcharts',
		loadChildren: () => import('./highcharts/highcharts.module').then(m => m.HighchartsModule)
	},
	{
		path: 'stocks-axis',
		loadChildren: () => import('./stocks-axis/stocks-axis.module').then(m => m.StocksAxisModule)
	},
	{
		path: 'stocks-prototype',
		loadChildren: () => import('./stocks-prototype/stocks-prototype.module').then(m => m.StocksPrototypeModule)
	},
	{
		path: 'canvasjs',
		loadChildren: () => import('./canvasjs/canvasjs.module').then(m => m.CanvasJSModule)
	},
	{
		path: 'shneider-canvasjs',
		loadChildren: () => import('./shneider-canvasjs/shneider-canvasjs.module').then(m => m.ShneiderCanvasjsModule)
	},
	{
		path: 'schneider-highcharts',
		loadChildren: () => import('./schneider-highcharts/schneider-highcharts.module').then(m => m.SchneiderHighchartsModule)
	},
	{
		path: 'schneider-chartjs',
		loadChildren: () => import('./schneider-chartjs/schneider-chartjs.module').then(m => m.SchneiderChartjsModule)
	},
	{
		path: 'schneider-plotly',
		loadChildren: () => import('./schneider-plotly/schneider-plotly.module').then(m => m.SchneiderPlotlyModule)
	},
	{
		path: 'schneider-apex',
		loadChildren: () => import('./schneider-apex/schneider-apex.module').then(m => m.SchneiderApexModule)
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
