import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SchneiderHighchartsComponent } from './schneider-highcharts.component';

const routes: Routes = [
	{
		path: '',
		component: SchneiderHighchartsComponent
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
	providers: []
})
export class SchneiderHighchartsRoutingModule { }
