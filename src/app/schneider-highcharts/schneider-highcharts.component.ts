import { Component, OnInit } from '@angular/core';
import { Chance } from 'chance';
import { DataService } from 'src/app/data.service';
import Highcharts, { color } from 'highcharts';

@Component({
	selector: 'app-schneider-highcharts',
	templateUrl: './schneider-highcharts.component.html',
	styleUrls: ['./schneider-highcharts.component.sass']
})
export class SchneiderHighchartsComponent implements OnInit {
	Highcharts: typeof Highcharts = Highcharts;
	chart0: Highcharts.Options;
	chart1: Highcharts.Options;
	chart2: Highcharts.Options;
	chart3: Highcharts.Options;
	chart4: Highcharts.Options;

	chance: Chance.Chance;

	// tslint:disable-next-line: max-line-length
	dyna1 = [[5, 80], [3, 150], [12, 350], [62, 330], [75, 305], [82, 290], [85, 90], [75, 50], [35, 65], [28, 50], [18, 70], [9, 45], [5, 80]];
	// tslint:disable-next-line: max-line-length
	dyna2 = [[2, 75], [3, 140], [12, 340], [62, 320], [75, 290], [82, 285], [85, 85], [75, 45], [35, 60], [28, 45], [18, 65], [8, 40], [2, 75]];
	// tslint:disable-next-line: max-line-length
	dyna3 = [[2, 70], [4, 130], [11, 330], [62, 310], [76, 280], [82, 275], [84, 75], [62, 35], [35, 50], [29, 40], [18, 65], [7, 35], [2, 70]];

	isLoaded = false;

	dynacards = [];

	constructor(private dataService: DataService) {
		this.chance = new Chance();

		dataService.getDynaData(10).subscribe((data: any[]) => {
			this.chart2.series = data.map(dynaData => ({
				type: 'scatter',
				data: dynaData
			}));

			this.isLoaded = true;
		});
	}

	ngOnInit() {

		this.loadDynacardGrid();

		this.chart0 = {
			title: {
				text: ''
			},
			credits: {
				enabled: false
			},
			chart: {
				type: 'line',
				// marginTop: 24,
				// spacingBottom: 30,
				// spacingLeft: 30,
				// spacingRight: 30,
				plotBorderColor: '#dddddd',
				panning: {
					enabled: true
				},
				zoomType: 'x',
				panKey: 'shift'
				// plotBorderWidth: 1
			},
			legend: {
				enabled: false
			},
			xAxis: {
				type: 'datetime',
				gridLineWidth: 1,
				gridLineColor: '#dddddd',
				tickWidth: 0,
				labels: {
					align: 'left',
					y: 12,
					style: {
						color: '#979797'
					}
				},
				showFirstLabel: true,
				showLastLabel: true,
				dateTimeLabelFormats: {
					hour: '%l:%M %P',
					day: '%l:%M %P'
				},

			},
			yAxis: [
				{
					title: {
						text: null
					},
					tickAmount: 4,
					tickLength: 0,
					tickWidth: 0,
					gridLineWidth: 1,
					gridLineColor: '#dddddd',
					labels: {
						style: {
							color: '#979797'
						}
					},
				}
			],
			plotOptions: {
				series: {
					marker: {
						enabled: false
					},
					lineWidth: 1,
					pointRange: 0
				}
			},
			series: [
				{
					animation: {
						duration: 2000,
						// Uses simple function
						// easing: easeOutBounce
					},
					data: Array.from({ length: 5000 }, () => this.chance.floating({ min: 0, max: 400 })),
					type: 'line',
					pointStart: Date.UTC(2010, 0, 1),
					pointInterval: 3600 * 1000 // one hour
				}
			]
		};

		this.chart1 = {
			title: {
				text: ''
			},
			credits: {
				enabled: false
			},
			chart: {
				type: 'line',
				marginTop: 24,
				spacingBottom: 30,
				spacingLeft: 30,
				spacingRight: 30,
				plotBorderColor: '#dddddd',
				// plotBorderWidth: 1,
			},
			legend: {
				enabled: false
			},
			xAxis: {
				type: 'datetime',
				gridLineWidth: 1,
				gridLineColor: '#dddddd',
				tickWidth: 0,
				labels: {
					align: 'left',
					y: 12,
					style: {
						color: '#979797'
					}
				},
				showFirstLabel: true,
				showLastLabel: true,
				dateTimeLabelFormats: {
					hour: '%l:%M %P',
					day: '%l:%M %P'
				},

			},
			yAxis: [
				{
					title: {
						text: null
					},
					tickAmount: 4,
					tickLength: 0,
					tickWidth: 0,
					gridLineWidth: 1,
					gridLineColor: '#dddddd',
					labels: {
						style: {
							color: '#979797'
						}
					},
				}
			],
			plotOptions: {
				series: {
					marker: {
						enabled: false
					},
					lineWidth: 1,
					pointRange: 0
				}
			},
			series: [
				{
					data: Array.from({ length: 24 }, () => this.chance.floating({ min: 0, max: 400 })),
					type: 'line',
					pointStart: Date.UTC(2010, 0, 1),
					pointInterval: 3600 * 1000, // one hour
					color: {
						linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
						stops: [
							[0, 'blue'],
							[0.5, 'yellow'],
							[1, 'red']
						]
					}
				},
				{
					data: Array.from({ length: 24 }, () => this.chance.integer({ min: 0, max: 10 })),
					type: 'line',
					pointStart: Date.UTC(2010, 0, 1),
					pointInterval: 3600 * 1000 // one hour
				},
				{
					data: Array.from({ length: 24 }, () => this.chance.integer({ min: 0, max: 350 })),
					type: 'line',
					pointStart: Date.UTC(2010, 0, 1),
					pointInterval: 3600 * 1000 // one hour
				},
				{
					data: Array.from({ length: 24 }, () => this.chance.integer({ min: 100, max: 125 })),
					type: 'line',
					pointStart: Date.UTC(2010, 0, 1),
					pointInterval: 3600 * 1000 // one hour
				}
			]
		};

		this.chart2 = {
			title: {
				text: ''
			},
			credits: {
				enabled: false
			},
			colors: ['rgba(0, 85, 188, 1)'],
			chart: {
				type: 'scatter',
				marginTop: 24,
				spacingBottom: 30,
				spacingLeft: 60,
				spacingRight: 60,
				plotBorderColor: '#dddddd',
				plotBorderWidth: 1,
				zoomType: 'xy'
			},
			legend: {
				enabled: false
			},
			xAxis: {
				title: {
					text: null
				},
				gridLineWidth: 1,
				gridLineColor: '#dddddd',
				tickWidth: 0,
				labels: {
					align: 'left',
					y: 12,
					style: {
						color: '#979797'
					}
				}
			},
			yAxis: {
				title: {
					text: null
				},
				min: 0,
				startOnTick: false,
				endOnTick: false,
				tickLength: 5,
				tickWidth: 1,
				gridLineWidth: 1,
				gridLineColor: '#dddddd',
				labels: {
					style: {
						color: '#979797'
					}
				},
			},
			plotOptions: {
				scatter: {
					marker: {
						enabled: false
					},
					// color: '#0055bc',
					pointRange: 0,
					linkedTo: ':previous',
					lineWidth: 2.5,
					events: {
						mouseOver: this.onMouseHover.bind(this),
						mouseOut: this.onMouseLeave.bind(this)
					}
				}
			},
			series: []
		};

		this.chart3 = {
			chart: {
				type: 'bar',
				marginTop: 30,
				spacingBottom: 30,
				spacingLeft: 30,
				spacingRight: 60,
			},
			title: {
				text: ''
			},
			credits: {
				enabled: false
			},
			legend: {
				enabled: false
			},
			xAxis: {
				categories: ['Fluid Pound', 'Gas Interference', 'Gas Lock', 'Normal', 'Plunger Stuck', 'Solids Grinding', 'Solids In Pump'],
				title: {
					text: null
				},
				labels: {
					align: 'left',
					reserveSpace: true
				}
			},
			yAxis: {
				title: {
					text: null
				},
				tickInterval: 100,
				labels: {
					align: 'left',
					y: 12,
					style: {
						color: '#979797'
					}
				},
				showLastLabel: false
			},
			series: [
				{
					data: [
						{ color: '#151eac', y: 680 },
						{ color: '#ffc400', y: 325 },
						{ color: '#0c688a', y: 450 },
						{ color: '#36b37e', y: 550 },
						{ color: '#333333', y: 210 },
						{ color: '#ac1574', y: 420 },
						{ color: '#de350b', y: 110 }
					],
					type: 'bar',
					pointWidth: 26
				}
			]
		};

		this.loadDynaChart();
	}

	onMouseHover(chart: any) {
		chart.target.update({
			color: 'red'
		});
	}

	onMouseLeave(chart: any) {
		chart.target.update({
			color: 'rgba(0, 85, 188, 1)'
		});
	}

	loadDynaChart() {
		this.chart4 = {
			title: {
				text: ''
			},
			credits: {
				enabled: false
			},
			colors: ['rgba(0, 85, 188, 1)'],
			chart: {
				events: {
					load: this.asyncLoad.bind(this)
				},
				type: 'scatter',
				marginTop: 24,
				spacingBottom: 30,
				spacingLeft: 60,
				spacingRight: 60,
				plotBorderColor: '#dddddd',
				plotBorderWidth: 1,
				zoomType: 'xy'
			},
			xAxis: {
				title: {
					text: null
				},
				gridLineWidth: 1,
				gridLineColor: '#dddddd',
				tickWidth: 0,
				labels: {
					align: 'left',
					y: 12,
					style: {
						color: '#979797'
					}
				}
			},
			yAxis: {
				title: {
					text: null
				},
				min: 0,
				startOnTick: false,
				endOnTick: false,
				tickLength: 5,
				tickWidth: 1,
				gridLineWidth: 1,
				gridLineColor: '#dddddd',
				labels: {
					style: {
						color: '#979797'
					}
				},
			},
			plotOptions: {
				scatter: {
					marker: {
						enabled: false
					},
					// color: '#0055bc',
					pointRange: 0,
					linkedTo: ':previous',
					lineWidth: 2.5,
					events: {
						mouseOver: this.onMouseHover.bind(this),
						mouseOut: this.onMouseLeave.bind(this)
					}
				}
			},
			series: [{ type: 'scatter' }]
		};
	}

	asyncLoad({ target }) {
		target.showLoading();
		this.dataService.getDynaData(10).subscribe((data: any[]) => {
			target.update({
				series: data.map(dynaData => ({
					type: 'scatter',
					data: dynaData
				}))
			}, true, true);
			target.hideLoading();
		});
	}

	loadDynacardGrid() {
		const x = [];
		const defaultOptions: Highcharts.Options = {
			title: {
				text: ''
			},
			credits: {
				enabled: false
			},
			legend: {
				enabled: false
			},
			xAxis: {
				visible: false
			},
			yAxis: {
				visible: false
			},
			tooltip: {
				enabled: false
			},
			plotOptions: {
				scatter: {
					marker: {
						enabled: false
					},
					color: '#212322',
					pointRange: 0,
					linkedTo: ':previous',
					lineWidth: 2.5
				}
			},
			series: []
		};

		this.dataService.getDynaData(100).subscribe((data: any[]) => {
			data.forEach(dynadata => {
				x.push({ ...defaultOptions, series: [{ type: 'scatter', data: dynadata }] });
			});
		});

		this.dynacards = x;
	}
}
