import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchneiderHighchartsComponent } from './schneider-highcharts.component';

describe('SchneiderHighchartsComponent', () => {
  let component: SchneiderHighchartsComponent;
  let fixture: ComponentFixture<SchneiderHighchartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchneiderHighchartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchneiderHighchartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
