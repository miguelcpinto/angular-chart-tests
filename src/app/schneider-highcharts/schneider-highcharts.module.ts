import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SchneiderHighchartsComponent } from './schneider-highcharts.component';
import { SchneiderHighchartsRoutingModule } from './schneider-highcharts-routing.module';
import { HighchartsChartModule } from 'highcharts-angular';

@NgModule({
	declarations: [SchneiderHighchartsComponent],
	imports: [
		CommonModule,
		HighchartsChartModule,
		SchneiderHighchartsRoutingModule
	]
})
export class SchneiderHighchartsModule { }
