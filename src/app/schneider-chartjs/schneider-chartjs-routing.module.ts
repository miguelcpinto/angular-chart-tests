import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SchneiderChartjsComponent } from './schneider-chartjs.component';

const routes: Routes = [
	{
		path: '',
		component: SchneiderChartjsComponent
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
	providers: []
})
export class SchneiderChartjsRoutingModule { }
