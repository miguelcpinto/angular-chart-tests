import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SchneiderChartjsRoutingModule } from './schneider-chartjs-routing.module';
import { SchneiderChartjsComponent } from './schneider-chartjs.component';

@NgModule({
	declarations: [SchneiderChartjsComponent],
	imports: [
		CommonModule,
		SchneiderChartjsRoutingModule
	]
})
export class SchneiderChartjsModule { }
