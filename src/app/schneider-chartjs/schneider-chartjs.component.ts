import { Component, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Chance } from 'chance';
import { Chart } from 'chart.js';
import moment from 'moment';

@Component({
	selector: 'app-schneider-chartjs',
	templateUrl: './schneider-chartjs.component.html',
	styleUrls: ['./schneider-chartjs.component.sass']
})
export class SchneiderChartjsComponent implements AfterViewInit {

	@ViewChild('initialLinearChart', { static: false }) chart1El: ElementRef;
	@ViewChild('dynaChart', { static: false }) dynaChart: ElementRef;
	@ViewChild('barChart', { static: false }) barChart: ElementRef;

	chart0: any;
	chart1: any;
	chart2: any;
	chart3: any;
	chance: Chance.Chance;

	// tslint:disable-next-line: max-line-length
	dyna1 = [[5, 80], [3, 150], [12, 350], [62, 330], [75, 305], [82, 290], [85, 90], [75, 50], [35, 65], [28, 50], [18, 70], [9, 45], [5, 80]];
	// tslint:disable-next-line: max-line-length
	dyna2 = [[2, 75], [3, 140], [12, 340], [62, 320], [75, 290], [82, 285], [85, 85], [75, 45], [35, 60], [28, 45], [18, 65], [8, 40], [2, 75]];
	// tslint:disable-next-line: max-line-length
	dyna3 = [[2, 70], [4, 130], [11, 330], [62, 310], [76, 280], [82, 275], [84, 75], [62, 35], [35, 50], [29, 40], [18, 65], [7, 35], [2, 70]];

	constructor() {
		this.chance = new Chance();
	}

	ngAfterViewInit(): void {
		this.chart1 = new Chart(document.getElementById('initialLinearChart'), {
			responsive: true,
			type: 'line',
			data: {
				labels: Array.from({ length: 24 }, (_, index) => moment().startOf('day').add(index, 'hour')),
				datasets: [
					{
						backgroundColor: '#2979ff',
						borderColor: '#2979ff',
						borderWidth: 2,
						pointRadius: 0,
						pointHitRadius: 5,
						fill: false,
						lineTension: 0,
						data: Array.from({ length: 24 }, () => this.chance.floating({ min: 0, max: 400 })),
					},
					{
						backgroundColor: '#29b6f6',
						borderColor: '#29b6f6',
						borderWidth: 2,
						pointRadius: 0,
						pointHitRadius: 5,
						fill: false,
						lineTension: 0,
						data: Array.from({ length: 24 }, () => this.chance.integer({ min: 0, max: 10 })),
					},
					{
						backgroundColor: '#2e7d32',
						borderColor: '#2e7d32',
						borderWidth: 2,
						pointRadius: 0,
						pointHitRadius: 5,
						fill: false,
						lineTension: 0,
						data: Array.from({ length: 24 }, () => this.chance.integer({ min: 0, max: 350 })),
					},
					{
						backgroundColor: '#ff8f00',
						borderColor: '#ff8f00',
						borderWidth: 2,
						pointRadius: 0,
						pointHitRadius: 5,
						fill: false,
						lineTension: 0,
						data: Array.from({ length: 24 }, () => this.chance.integer({ min: 100, max: 125 })),
					}
				]
			},
			options: {
				animation: {
					easing: 'easeOutCubic'
				},
				layout: {
					padding: {
						left: 15,
						right: 30,
						top: 20,
						bottom: 30
					}
				},
				legend: {
					display: false
				},
				scales: {
					xAxes: [{
						type: 'time',
						distribution: 'series',
						time: {
							unit: 'minute'
						},
						gridLines: {
							color: '#e9ebf1'
						},
						ticks: {
							fontFamily: 'Lucida Grande',
							fontColor: '#979797',
							fontSize: 10,
							labelOffset: 24,
							padding: -5,
							callback: (tick) => {
								return tick === '11:00 pm' ? '' : tick;
							}
						}
					}],
					yAxes: [{
						gridLines: {
							color: '#e9ebf1',
						},
						ticks: {
							fontFamily: 'Lucida Grande',
							fontColor: '#979797',
							fontSize: 10,
							maxTicksLimit: 4,
							padding: 10
						}
					}]
				}
			}
		});

		this.chart2 = new Chart(this.dynaChart.nativeElement, {
			type: 'scatter',
			responsive: true,
			data: {
				datasets: [
					{
						backgroundColor: 'rgba(0, 85, 188, 1)',
						borderColor: 'rgba(0, 85, 188, 1)',
						showLine: true,
						fill: false,
						lineTension: 0,
						pointRadius: 1,
						pointHitRadius: 5,
						data: this.dyna1.map(ax => ({ x: ax[0], y: ax[1] }))
					}, {
						backgroundColor: 'rgba(0, 85, 188, 0.8)',
						borderColor: 'rgba(0, 85, 188, 0.8)',
						showLine: true,
						fill: false,
						lineTension: 0,
						pointRadius: 1,
						pointHitRadius: 5,
						data: this.dyna2.map(ax => ({ x: ax[0], y: ax[1] }))
					}, {
						backgroundColor: 'rgba(0, 85, 188, 0.6)',
						borderColor: 'rgba(0, 85, 188, 0.6)',
						showLine: true,
						fill: false,
						lineTension: 0,
						pointRadius: 1,
						pointHitRadius: 5,
						data: this.dyna3.map(ax => ({ x: ax[0], y: ax[1] }))
					}
				]
			},
			options: {
				animation: {
					easing: 'easeOutCubic'
				},
				layout: {
					padding: {
						left: 15,
						right: 30,
						top: 20,
						bottom: 30
					}
				},
				legend: {
					display: false
				},
				scales: {
					xAxes: [{
						type: 'linear',
						gridLines: {
							color: '#e9ebf1'
						},
						ticks: {
							beginAtZero: true,
							fontFamily: 'Lucida Grande',
							fontColor: '#979797',
							fontSize: 10,
							labelOffset: 10,
							padding: -5,
							callback: (tick, index, data) => {
								return index + 1 === data.length ? '' : tick;
							}
						}
					}],
					yAxes: [{
						gridLines: {
							color: '#e9ebf1',
						},
						ticks: {
							beginAtZero: true,
							fontFamily: 'Lucida Grande',
							fontColor: '#979797',
							fontSize: 10,
							maxTicksLimit: 4,
							padding: 10
						}
					}]
				}
			}
		});

		this.chart3 = new Chart(document.getElementById('barChart'), {
			type: 'horizontalBar',
			responsive: true,
			data: {
				labels: ['Fluid Pound', 'Gas Interference', 'Gas Lock', 'Normal', 'Plunger Stuck', 'Solids Grinding', 'Solids In Pump'],
				borderSkipped: 'false',
				datasets: [{
					data: [680, 325, 450, 550, 210, 420, 110],
					backgroundColor: ['#151eac', '#ffc400', '#0c688a', '#36b37e', '#333333', '#ac1574', '#de350b']
				}]
			},
			options: {
				responsive: true,
				animation: {
					easing: 'easeOutCubic'
				},
				layout: {
					padding: {
						left: 15,
						right: 30,
						top: 20,
						bottom: 30
					}
				},
				legend: {
					display: false
				},
				scales: {
					yAxes: [{
						position: 'left',
						gridLines: {
							display: false
						},
						ticks: {
							fontFamily: 'Lucida Grande',
							fontColor: '#212322',
							fontSize: 12,
							padding: 10,
							labelOffset: 0
						},
						barThickness: 25
					}],
					xAxes: [{
						gridLines: {
							color: '#e9ebf1',
						},
						ticks: {
							beginAtZero: true,
							fontFamily: 'Lucida Grande',
							fontColor: '#979797',
							fontSize: 10,
							labelOffset: 12,
							padding: -5,
							callback: (tick, index, data) => {
								return index + 1 === data.length ? '' : tick;
							}
						}
					}]
				}

			}
		});

		this.chart0 = new Chart(document.getElementById('5000PointsChart'), {
			responsive: true,
			type: 'line',
			data: {
				labels: Array.from({ length: 5000 }, (_, index) => moment().startOf('day').add(index, 'seconds')),
				datasets: [
					{
						backgroundColor: '#2979ff',
						borderColor: '#2979ff',
						borderWidth: 2,
						pointRadius: 0,
						pointHitRadius: 5,
						fill: false,
						lineTension: 0,
						data: Array.from({ length: 5000 }, () => this.chance.floating({ min: 0, max: 400 })),
					}
				]
			},
			options: {
				animation: {
					easing: 'easeOutCubic'
				},
				layout: {
					padding: {
						left: 15,
						right: 30,
						top: 20,
						bottom: 30
					}
				},
				legend: {
					display: false
				},
				scales: {
					xAxes: [{
						type: 'time',
						distribution: 'series',
						time: {
							unit: 'minute'
						},
						gridLines: {
							color: '#e9ebf1'
						},
						ticks: {
							fontFamily: 'Lucida Grande',
							fontColor: '#979797',
							fontSize: 10,
							labelOffset: 24,
							padding: -5,
							callback: (tick) => {
								return tick === '11:00 pm' ? '' : tick;
							}
						}
					}],
					yAxes: [{
						gridLines: {
							color: '#e9ebf1',
						},
						ticks: {
							fontFamily: 'Lucida Grande',
							fontColor: '#979797',
							fontSize: 10,
							maxTicksLimit: 4,
							padding: 10
						}
					}]
				}
			}

		});
	}
}
