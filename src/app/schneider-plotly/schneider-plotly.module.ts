import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SchneiderPlotlyComponent } from './schneider-plotly.component';
import { SchneiderPlotlyRoutingModule } from './schneider-plotly-routing.module';

import * as PlotlyJS from 'plotly.js/dist/plotly.js';
import { PlotlyModule } from 'angular-plotly.js';

PlotlyModule.plotlyjs = PlotlyJS;

@NgModule({
	declarations: [SchneiderPlotlyComponent],
	imports: [
		CommonModule,
		SchneiderPlotlyRoutingModule,
		PlotlyModule
	]
})
export class SchneiderPlotlyModule { }
