import { Component, OnInit } from '@angular/core';
import { Chance } from 'chance';
import moment from 'moment';

@Component({
	selector: 'app-schneider-plotly',
	templateUrl: './schneider-plotly.component.html',
	styleUrls: ['./schneider-plotly.component.sass']
})
export class SchneiderPlotlyComponent implements OnInit {
	chance: Chance.Chance;

	chart0;
	chart1;
	chart2;
	chart3;

	// tslint:disable-next-line: max-line-length
	dyna1 = [[5, 80], [3, 150], [12, 350], [62, 330], [75, 305], [82, 290], [85, 90], [75, 50], [35, 65], [28, 50], [18, 70], [9, 45], [5, 80]];
	// tslint:disable-next-line: max-line-length
	dyna2 = [[2, 75], [3, 140], [12, 340], [62, 320], [75, 290], [82, 285], [85, 85], [75, 45], [35, 60], [28, 45], [18, 65], [8, 40], [2, 75]];
	// tslint:disable-next-line: max-line-length
	dyna3 = [[2, 70], [4, 130], [11, 330], [62, 310], [76, 280], [82, 275], [84, 75], [62, 35], [35, 50], [29, 40], [18, 65], [7, 35], [2, 70]];


	constructor() {
		this.chance = new Chance();
	}

	ngOnInit() {
		// const time = Array.from({ length: 24 }, (_, index) => moment().startOf('day').add(index, 'hour'));
		const time = Array.from({ length: 24 }, (_, index) => moment().startOf('day').add(index, 'hour').toDate());

		this.chart1 = {
			data: [
				{
					x: time,
					y: Array.from({ length: 24 }, () => this.chance.floating({ min: 0, max: 400 })),
					type: 'scatter',
					mode: 'lines',
					name: 'Fluid Pound',
					line: {
						color: '#2979ff',
						width: 1
					}
				},
				{
					x: time,
					y: Array.from({ length: 24 }, () => this.chance.integer({ min: 0, max: 10 })),
					type: 'scatter',
					mode: 'lines',
					name: 'Gas Lock',
					line: {
						color: '#29b6f6',
						width: 1
					}
				},
				{
					x: time,
					y: Array.from({ length: 24 }, () => this.chance.integer({ min: 0, max: 350 })),
					type: 'scatter',
					mode: 'lines',
					name: 'Gas Lock',
					line: {
						color: '#2e7d32',
						width: 1
					}
				},
			],
			layout: {
				showlegend: false,
				xaxis: {
					type: 'date',
					tickformat: '%I:%M %p',
					ticklen: 0,
					tickfont: {
						family: 'Lucida Grande',
						size: 10,
						color: '#979797'
					},
					gridcolor: '#e9ebf1',
					zeroline: false,
					linecolor: '#e9ebf1',
					linewidth: 1,
					mirror: true,
					stickinside: true
				},
				yaxis: {
					zeroline: false,
					ticklen: 0,
					nticks: 4,
					tickColor: '#e9ebf1',
					tickfont: {
						family: 'Lucida Grande',
						size: 10,
						color: '#979797'
					},
					gridcolor: '#e9ebf1',
					linecolor: '#e9ebf1',
					linewidth: 1,
					mirror: true
				}
			},
			config: {
				displayModeBar: false,
				responsive: true
			}
		};

		this.chart2 = {
			data: [
				{
					x: this.dyna1.map(x => x[0]),
					y: this.dyna1.map(y => y[1]),
					type: 'scatter',
					mode: 'lines',
					line: {
						width: 2
					}
				},
				{
					x: this.dyna2.map(x => x[0]),
					y: this.dyna2.map(y => y[1]),
					type: 'scatter',
					mode: 'lines',
					line: {
						width: 2
					}
				},
				{
					x: this.dyna3.map(x => x[0]),
					y: this.dyna3.map(y => y[1]),
					type: 'scatter',
					mode: 'lines',
					line: {
						width: 2
					},

				},
			],
			layout: {
				colorway: ['rgba(0, 85, 188, 1)', 'rgba(0, 85, 188, .8)', 'rgba(0, 85, 188, .6)', 'rgba(0, 85, 188, .4)'],
				showlegend: false,
				margin: {
					t: 20,
					b: 40,
				},
				xaxis: {
					ticklen: 0,
					tickfont: {
						family: 'Lucida Grande',
						size: 10,
						color: '#979797'
					},
					gridcolor: '#e9ebf1',
					zeroline: false,
					linecolor: '#e9ebf1',
					linewidth: 1,
					mirror: true,
				},
				yaxis: {
					zeroline: false,
					ticklen: 0,
					nticks: 4,
					tickColor: '#e9ebf1',
					tickfont: {
						family: 'Lucida Grande',
						size: 10,
						color: '#979797'
					},
					gridcolor: '#e9ebf1',
					linecolor: '#e9ebf1',
					linewidth: 1,
					mirror: true
				}
			},
			config: {
				displayModeBar: false,
				responsive: true
			}
		};

		this.chart3 = {
			data: [{
				type: 'bar',
				orientation: 'h',
				y: ['Fluid Pound', 'Gas Interference', 'Gas Lock', 'Normal', 'Plunger Stuck', 'Solids Grinding', 'Solids In Pump'],
				x: [680, 325, 450, 550, 210, 420, 110],
				marker: {
					color: ['#151eac', '#ffc400', '#0c688a', '#36b37e', '#333333', '#ac1574', '#de350b'],
				}
			}],
			layout: {
				margin: {
					l: 150,
					t: 20,
					b: 40,
					r: 150
				},
				xaxis: {
					ticklen: 0,
					tickfont: {
						family: 'Lucida Grande',
						size: 10,
						color: '#979797'
					},
					gridcolor: '#e9ebf1',
					zeroline: false,
					linecolor: '#e9ebf1',
					linewidth: 0,
				},
				yaxis: {
					zeroline: false,
					tickColor: '#e9ebf1',
					tickfont: {
						family: 'Lucida Grande',
						size: 12,
						color: '#212322'
					},
					gridcolor: '#e9ebf1',
					linecolor: '#e9ebf1',
					linewidth: 1,
					showgrid: false,
					align: 'left'
				}
			},
			config: {
				displayModeBar: false,
				responsive: true
			}
		};


		this.chart0 = {
			data: [
				{
					x: Array.from({ length: 5000 }, (_, index) => moment().startOf('day').add(index, 'seconds').toDate()),
					y: Array.from({ length: 5000 }, () => this.chance.floating({ min: 0, max: 400 })),
					type: 'scatter',
					mode: 'lines',
					name: 'Fluid Pound',
					line: {
						color: '#2979ff',
						width: 1
					}
				}
			],
			layout: {
				showlegend: false,
				xaxis: {
					type: 'date',
					tickformat: '%I:%M %p',
					ticklen: 0,
					tickfont: {
						family: 'Lucida Grande',
						size: 10,
						color: '#979797'
					},
					gridcolor: '#e9ebf1',
					zeroline: false,
					linecolor: '#e9ebf1',
					linewidth: 1,
					mirror: true,
					stickinside: true
				},
				yaxis: {
					zeroline: false,
					ticklen: 0,
					nticks: 4,
					tickColor: '#e9ebf1',
					tickfont: {
						family: 'Lucida Grande',
						size: 10,
						color: '#979797'
					},
					gridcolor: '#e9ebf1',
					linecolor: '#e9ebf1',
					linewidth: 1,
					mirror: true
				}
			},
			config: {
				displayModeBar: false,
				responsive: true
			}
		};
	}

}
