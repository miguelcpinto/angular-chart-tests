import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SchneiderPlotlyComponent } from './schneider-plotly.component';

const routes: Routes = [
	{
		path: '',
		component: SchneiderPlotlyComponent
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
	providers: []
})
export class SchneiderPlotlyRoutingModule { }
