import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchneiderPlotlyComponent } from './schneider-plotly.component';

describe('SchneiderPlotlyComponent', () => {
  let component: SchneiderPlotlyComponent;
  let fixture: ComponentFixture<SchneiderPlotlyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchneiderPlotlyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchneiderPlotlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
