import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HighchartsComponent } from './highcharts.component';
import { HighchartsRoutingModule } from './highcharts-routing.module';
import { HighchartsChartModule } from 'highcharts-angular';

@NgModule({
    imports: [
        CommonModule,
        HighchartsChartModule,
        HighchartsRoutingModule
    ],
    declarations: [HighchartsComponent]
})
export class HighchartsModule { }
