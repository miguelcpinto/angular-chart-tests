import { Component, OnInit } from '@angular/core';
import { Chance } from 'chance';
import * as Highcharts from 'highcharts';
import * as HighchartsStock from 'highcharts/highstock';
import { webSocket } from 'rxjs/webSocket';

@Component({
	selector: 'app-highcharts',
	templateUrl: './highcharts.component.html',
	styleUrls: ['./highcharts.component.sass']
})
export class HighchartsComponent implements OnInit {
	chance: Chance.Chance;

	Highcharts: typeof Highcharts = Highcharts;
	HighchartsStock: typeof Highcharts = HighchartsStock;

	chart1: Highcharts.Options;
	chart2: Highcharts.Options;
	chart3: Highcharts.Options;
	chart4: Highcharts.Options;
	chart5: Highcharts.Options;
	chart6: Highcharts.Options;
	chart7: Highcharts.Options;
	chart8: Highcharts.Options;
	chart9: Highcharts.Options;

	constructor() {
		this.chance = new Chance();
	}

	ngOnInit() {
		let i = 0;
		const date = new Date().getTime();
		this.chart1 = {
			title: {
				text: ''
			},
			chart: {
				panning: {
					enabled: true
				},
				zoomType: 'x',
				panKey: 'shift'
			},
			series: [{
				type: 'line',
				data: Array.from({ length: 5000 }, () => [date + (600000 * i++), this.chance.floating({ min: 0, max: 100 })]),
			}]
		};

		this.chart2 = {
			title: {
				text: ''
			},
			yAxis: [
				{
					title: {
						align: 'high',
						text: 'Bar',
						style: {
							color: 'rgb(124, 181, 236)'
						}
					},
					labels: {
						style: {
							color: 'rgb(124, 181, 236)'
						}
					}
				}, {
					title: {
						text: 'M'
					},
					opposite: true
				}, {
					min: 0,
					max: 5,
					title: {
						text: ''
					},
					labels: {
						format: '{value} kw/h'
					}
				}
			],
			series: [
				{
					yAxis: 0,
					name: 'Pressure',
					data: Array.from({ length: 10 }, () => this.chance.floating({ min: 0, max: 100 })),
					type: 'line'
				}, {
					yAxis: 1,
					name: 'Width',
					data: Array.from({ length: 10 }, () => this.chance.floating({ min: 0, max: 20 })),
					type: 'line'
				}, {
					yAxis: 2,
					name: 'Enery',
					data: Array.from({ length: 10 }, () => this.chance.floating({ min: 0, max: 5 })),
					type: 'spline',
					dashStyle: 'Dot',
					marker: {
						enabled: false
					},
					lineWidth: 3
				}
			]
		};

		this.chart3 = {
			xAxis: {
				gridLineWidth: 1
			},
			series: [
				{
					name: 'Pressure',
					data: [4, 3, null, 1, 8, null, null, 9, 8, 5],
					type: 'line'
				}, {
					name: 'EMpty',
					data: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
					type: 'line'
				}
			]
		};

		this.chart4 = {
			title: {
				text: ''
			},
			data: {
				enablePolling: true
			},
			chart: {
				zoomType: 'x',
				events: {
					load: this.requestSocketData.bind(this)
				}
			},
			lang: {
				noData: 'No data available'
			},
			noData: {
				attr: {
					zIndex: 9999
				},
				style: {
					fontWeight: 'bold',
					fontSize: '15px',
					color: '#303030'
				}
			},
			series: [
				{
					type: 'line',
					data: []
				}
			]
		};

		this.chart5 = {
			...this.chart3,
			tooltip: {
				className: 'tooltip-wrapper',
				backgroundColor: '#dee',
				borderColor: '#dee',
				style: {
					width: 500
				},
				shadow: false,
				shared: true,
				useHTML: true,
				headerFormat: '<div class="tooltip-wrapper"><div class="inline header-title">Good{point.key}</div>',
				pointFormat: `
					<div class="inline">
						<div class="left">
							<span style="color:{point.color}">●</span>
							{series.name}</div>
						<div class="">{point.y}</div>
					</div>`,
				footerFormat: '</div></div>',
				valueDecimals: 2
			},
		};

		this.chart6 = {
			title: {
				text: ''
			},
			credits: {
				enabled: false
			},
			xAxis: {
				plotBands: [
					{
						from: 0,
						to: 3,
						color: '#7b93a9',
						label: {
							text: '<b>Stage 1</b>',
							style: {
								color: '#fff'
							}
						}
					}, {
						from: 5,
						to: 9,
						color: '#d6948f',
						label: {
							text: '<b>Stage 2</b>',
							style: {
								color: '#fff'
							}
						}
					}
				]
			},
			series: [
				{
					data: Array.from({ length: 10 }, () => this.chance.floating({ min: 0, max: 100 })),
					type: 'line'
				}, {
					data: Array.from({ length: 10 }, () => this.chance.floating({ min: 0, max: 100 })),
					type: 'column'
				}
			]
		};

		this.chart7 = {
			chart: {
				type: 'scatter',
				animation: true,
				zoomType: 'x'
			},
			title: {
				text: ''
			},
			xAxis: {
				// min: 0,
				// max: 5
			},
			yAxis: {
				min: 0,
				startOnTick: false,
				endOnTick: false
			},
			series: [
				{
					marker: {
						enabled: true,
						// radius: 1
					},
					data: [[1, 1], [1, 2], [2, 2.5], [3, 2.5], [3.25, 1.75], [2, 1.5], [1.5, 1.5], [1, 1]],
					type: 'scatter',
					// stickyTracking: false,
					linkedTo: ':previous',
					lineWidth: 2,
				},
				{
					marker: {
						enabled: true,
						// radius: 1
					},
					data: [[1.15, 1.1], [1.15, 2], [2, 2.5], [3.1, 2.5], [3.3, 1.75], [2, 1.55], [1.5, 1.5], [1.15, 1.1]],
					type: 'scatter',
					// stickyTracking: false,
					linkedTo: ':previous',
					lineWidth: 2,
				}
			],
			tooltip: {
				useHTML: true,
				followPointer: true,
				followTouchMove: true,
				headerFormat: '<div class="tooltip-wrapper">',
				pointFormat: `
					<div class="inline">
						<div class="left">{point.x} / {point.y}</div>
					</div>`,
				footerFormat: '</div>',
			}
		};

		const defaultSelected = [2, 5, 8];
		const randomList = Array.from({ length: 15 }, () => this.chance.integer({ min: 0, max: 10 }));
		const data = randomList.map(value => ({
			y: value,
			selected: defaultSelected.includes(value)
		}));
		this.chart8 = {
			title: {
				text: ''
			},
			series: [
				{
					allowPointSelect: true,
					data,
					type: 'line',
					point: {
						events: {
							select: this.handleSelectedPoint.bind(this)
						}
					}
				}
			]
		};

		this.chart9 = {
			title: {
				text: ''
			},
			xAxis: {
				crosshair: true,
				tickWidth: 0,
				gridLineWidth: 1,
				labels: {
					align: 'left',
					x: 3,
					y: -3
				}
			},
			tooltip: {
				shared: true
			},
			legend: {
				align: 'left',
				verticalAlign: 'top',
				borderWidth: 0
			},
			plotOptions: {
				line: {
					cursor: 'pointer',
					point: {
						events: {
							click: (e) => {
								console.log(e);
							}
						}
					},
					lineWidth: 2,
					states: {
						hover: {
							lineWidth: 5
						}
					},
					marker: {
						lineWidth: 1
					}
				}
			},
			series: [
				{
					allowPointSelect: true,
					data: Array.from({ length: 20 }, () => this.chance.integer({ min: 0, max: 10 })),
					type: 'line'
				}, {
					allowPointSelect: true,
					data: Array.from({ length: 20 }, () => this.chance.integer({ min: 0, max: 10 })),
					type: 'line'
				}
			]
		};
	}

	requestSocketData(c) {
		const socket$ = webSocket('ws://localhost:8999');
		socket$
			.subscribe(
				(value) => {
					c.target.series[0].addPoint(value);
				},
				(err) => console.error(err),
				() => console.warn('Completed!')
			);
	}

	handleSelectedPoint() {
		console.log('asdasd');

	}
}
