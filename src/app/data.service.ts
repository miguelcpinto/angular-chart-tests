import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const BASE_URL = 'http://localhost:8999';

@Injectable({
	providedIn: 'root'
})
export class DataService {
	constructor(private http: HttpClient) { }

	getDynaData(howMany?: number) {
		return this.http.get(`${BASE_URL}/dyna/${howMany}`);
	}
}
