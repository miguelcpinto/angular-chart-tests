import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import * as CanvasJS from '@assets/canvasjs.min.js';
import Canvas from 'canvasjs';
import { Chance } from 'chance';
import { DataService } from '../data.service';

@Component({
	selector: 'app-shneider-canvasjs',
	templateUrl: './shneider-canvasjs.component.html',
	styleUrls: ['./shneider-canvasjs.component.sass']
})
export class ShneiderCanvasjsComponent implements OnInit {
	chance: Chance.Chance;

	@ViewChild('gridItems', { static: true }) gridItems: ElementRef;

	// tslint:disable-next-line: max-line-length
	dyna1 = [[5, 80], [3, 150], [12, 350], [62, 330], [75, 305], [82, 290], [85, 90], [75, 50], [35, 65], [28, 50], [18, 70], [9, 45], [5, 80]];
	// tslint:disable-next-line: max-line-length
	dyna2 = [[2, 75], [3, 140], [12, 340], [62, 320], [75, 290], [82, 285], [85, 85], [75, 45], [35, 60], [28, 45], [18, 65], [8, 40], [2, 75]];
	// tslint:disable-next-line: max-line-length
	dyna3 = [[2, 70], [4, 130], [11, 330], [62, 310], [76, 280], [82, 275], [84, 75], [62, 35], [35, 50], [29, 40], [18, 65], [7, 35], [2, 70]];

	dynacards = [];

	constructor(private dataService: DataService) {
		this.chance = new Chance();

		CanvasJS.addColorSet(
			'customColorSet1',
			[
				'#de350b',
				'#ac1574',
				'#333333',
				'#36b37e',
				'#0c688a',
				'#ffc400',
				'#151eac'
			]);

		CanvasJS.addColorSet(
			'customColorSet2',
			[
				'rgba(0, 85, 188, 1)',
				'rgba(0, 85, 188, .8)',
				'rgba(0, 85, 188, .6)',
				'rgba(0, 85, 188, .4)',
				'rgba(0, 85, 188, .2)',
				'rgba(0, 85, 188, .1)',
			]);

		dataService.getDynaData(10).subscribe((data: any[]) => {
			this.createFirstDynacard(data);
		});
	}

	ngOnInit() {
		// this.create5000PointsChart();
		// this.createDataPointsChart();
		// this.createBarChart();
		// this.createSndDynacard();
		this.loadDynacardGrid();
	}

	createDataPointsChart() {
		const chart = new CanvasJS.Chart('dataPointsContainer', {
			exportEnabled: true,
			colorSet: 'customColorSet1',
			animationEnabled: true,
			zoomEnabled: true,
			title: {
				text: ''
			},
			toolTip: {
				shared: true,
				cornerRadius: 4
			},
			rangeChanged: (e) => {
				console.log(`
					type : ${e.type},
					trigger : ${e.trigger},
					AxisX viewportMininum : ${e.axisX[0].viewportMinimum},
					AxisX viewportMaximum : ${e.axisX[0].viewportMaximum}
				`);
			},
			rangeChanging: (e) => {
				console.log(`
					type : ${e.type},
					trigger : ${e.trigger},
					AxisX viewportMininum : ${e.axisX[0].viewportMinimum},
					AxisX viewportMaximum : ${e.axisX[0].viewportMaximum}
				`);
			},
			axisX: {
				labelFontColor: '#979797',
				labelFontSize: 10,
				gridThickness: 1,
				gridColor: '#e9ebf1',
				lineColor: '#e9ebf1',
				lineThickness: 1,
				labelFormatter: e => `${e.value} am`,
				tickColor: 'white',
				crosshair: {
					snapToDataPoint: true,
					enabled: true,
					lineDashType: 'solid',
					opacity: 0.6,
					thickness: 2,
					labelFormatter: () => ''
				},
				includeZero: false,
				minimum: 0
			},
			axisY: {
				tickColor: 'white',
				labelFontColor: '#979797',
				labelFontSize: 10,
				gridThickness: 1,
				gridColor: '#e9ebf1',
				lineColor: '#e9ebf1',
				lineThickness: 1,
				label: '',
				interval: 100
			},
			data: [{
				type: 'line',
				name: 'Fluid Pound',
				markerType: 'none',
				dataPoints: Array.from({ length: 50 }, () => ({ y: this.chance.floating({ min: 0, max: 400 }) })),
			}, {
				type: 'line',
				markerType: 'none',
				name: 'Gas Interference',
				dataPoints: Array.from({ length: 50 }, () => ({ y: this.chance.floating({ min: 50, max: 100 }) })),
			}, {
				type: 'line',
				markerType: 'none',
				name: 'Gas Lock',
				dataPoints: Array.from({ length: 50 }, () => ({ y: this.chance.floating({ min: 200, max: 400 }) })),
			}, {
				type: 'line',
				markerType: 'none',
				name: 'Normal',
				dataPoints: Array.from({ length: 50 }, () => ({ y: this.chance.floating({ min: 0, max: 25 }) })),
			}, {
				type: 'Plunger Stuck',
				markerType: 'none',
				dataPoints: Array.from({ length: 50 }, () => ({ y: this.chance.floating({ min: 50, max: 75 }) })),
			}]
		}) as Canvas.Chart;

		chart.render();
	}

	createBarChart() {
		const chart = new CanvasJS.Chart('barChartContainer', {
			exportEnabled: true,
			animationEnabled: true,
			colorSet: 'customColorSet1',
			title: {
				text: ''
			},
			rangeChanged: (e) => {
				console.log(`
					type : ${e.type},
					trigger : ${e.trigger},
					AxisX viewportMininum : ${e.axisX[0].viewportMinimum},
					AxisX viewportMaximum : ${e.axisX[0].viewportMaximum}
				`);
			},
			rangeChanging: (e) => {
				console.log(`
					type : ${e.type},
					trigger : ${e.trigger},
					AxisX viewportMininum : ${e.axisX[0].viewportMinimum},
					AxisX viewportMaximum : ${e.axisX[0].viewportMaximum}
				`);
			},
			dataPointMaxWidth: 26,
			axisX: {
				labelFontColor: '#212322',
				labelFontSize: 12,
				lineColor: '#e9ebf1',
				lineThickness: 1,
				tickColor: 'white',
				tickLength: 10,
				includeZero: false
			},
			axisY: {
				tickColor: 'white',
				labelFontColor: '#979797',
				labelFontSize: 10,
				gridThickness: 1,
				gridColor: '#e9ebf1',
				lineColor: '#e9ebf1',
				lineThickness: 0,
				label: '',
				interval: 10
			},
			data: [{
				type: 'bar',
				dataPoints: [
					{ label: 'Fluid Pound', y: 17.8 },
					{ label: 'Gas Interference', y: 22.8 },
					{ label: 'Gas Lock', y: 22.8 },
					{ label: 'Normal', y: 24.3 },
					{ label: 'Plunger Stuck', y: 36.8 },
					{ label: 'Solids Griding', y: 41.1 },
					{ label: 'Solids in Pum', y: 46.1 }
				]
			}]
		});
		chart.render();
	}

	createFirstDynacard(values: any[]) {
		const data = values.map((dynaData, index) => ({
			mouseover: this.onDynacardMouseOver.bind(this),
			mouseout: this.onDynacardMouseLeave.bind(this),
			type: 'line',
			lineThickness: 3,
			lineColor: 'rgba(0, 85, 188, .1)',
			markerSize: 0,
			dataPoints: dynaData.map(([x, y]) => ({ x, y }))
		}));

		const chart = new CanvasJS.Chart('dynacardChart', {
			animationEnabled: true,
			// colorSet: 'customColorSet1',
			zoomEnabled: true,
			zoomType: 'xy',
			title: {
				text: ''
			},
			axisX: {
				labelFontColor: '#979797',
				labelFontSize: 10,
				labelFontFamily: 'Lucida Grande',
				tickColor: '#e9ebf1',
				gridThickness: 1,
				gridColor: '#e9ebf1',
				lineColor: '#e9ebf1',
				labelAngle: 180,
				viewportMinimum: -1
			},
			axisY: {
				labelFontColor: '#979797',
				labelFontSize: 10,
				labelFontFamily: 'Lucida Grande',
				tickColor: '#e9ebf1',
				gridColor: '#e9ebf1',
				lineColor: '#e9ebf1',
				viewportMinimum: -1
			},
			data
		});
		chart.render();
	}

	onDynacardMouseOver(x) {
		console.log(x);
		x.dataSeries.color = 'red';
		x.dataSeries.lineColor = 'red';

		x.chart.render();
	}

	onDynacardMouseLeave(x) {
		x.dataSeries.color = 'rgba(0, 85, 188, .25)';
		x.dataSeries.lineColor = 'rgba(0, 85, 188, .25)';
		x.chart.render();
	}

	createSndDynacard() {
		const options = {
			animationEnabled: true,
			colorSet: 'customColorSet2',
			zoomEnabled: true,
			zoomType: 'xy',
			title: {
				text: ''
			},
			subtitles: [{
				text: 'loading ...',
				verticalAlign: 'center',
			}],
			axisX: {
				labelFontColor: '#979797',
				labelFontSize: 10,
				labelFontFamily: 'Lucida Grande',
				tickColor: '#e9ebf1',
				gridThickness: 1,
				gridColor: '#e9ebf1',
				lineColor: '#e9ebf1',
				labelAngle: 180,
				viewportMinimum: -1
			},
			axisY: {
				labelFontColor: '#979797',
				labelFontSize: 10,
				labelFontFamily: 'Lucida Grande',
				tickColor: '#e9ebf1',
				gridColor: '#e9ebf1',
				lineColor: '#e9ebf1',
			},
			data: [{ type: 'line' }]
		};

		// i need that to animate the chart in the second rendering
		let chart = new CanvasJS.Chart('dynacardChart2', options);
		chart.render();

		this.dataService.getDynaData(10).subscribe((data: any[]) => {
			chart = new CanvasJS.Chart('dynacardChart2', options);
			chart.options.subtitles = [];
			chart.options.data = data.map(dynaData => ({
				type: 'line',
				lineThickness: 3,
				markerSize: 0,
				dataPoints: dynaData.map(([x, y]) => ({ x, y }))
			}));

			const startTime: any = new Date();
			chart.render();
			const endTime: any = new Date();
			console.log(endTime - startTime);

		});
	}

	create5000PointsChart() {
		const chart = new CanvasJS.Chart('chart5000Points', {
			exportEnabled: false,
			colorSet: 'customColorSet1',
			animationEnabled: true,
			zoomEnabled: true,
			title: {
				text: ''
			},
			toolTip: {
				shared: true,
				cornerRadius: 4
			},
			rangeChanged: (e) => {
				console.log(`
					type : ${e.type},
					trigger : ${e.trigger},
					AxisX viewportMininum : ${e.axisX[0].viewportMinimum},
					AxisX viewportMaximum : ${e.axisX[0].viewportMaximum}
				`);
			},
			rangeChanging: (e) => {
				console.log(`
					type : ${e.type},
					trigger : ${e.trigger},
					AxisX viewportMininum : ${e.axisX[0].viewportMinimum},
					AxisX viewportMaximum : ${e.axisX[0].viewportMaximum}
				`);
			},
			axisX: {
				labelFontColor: '#979797',
				labelFontSize: 10,
				gridThickness: 1,
				gridColor: '#e9ebf1',
				lineColor: '#e9ebf1',
				lineThickness: 1,
				labelFormatter: e => `${e.value} am`,
				tickColor: 'white',
				crosshair: {
					snapToDataPoint: true,
					enabled: true,
					lineDashType: 'solid',
					opacity: 0.6,
					thickness: 2,
					labelFormatter: () => ''
				},
				includeZero: false,
				minimum: 0
			},
			axisY: {
				tickColor: 'white',
				labelFontColor: '#979797',
				labelFontSize: 10,
				gridThickness: 1,
				gridColor: '#e9ebf1',
				lineColor: '#e9ebf1',
				lineThickness: 1,
				label: '',
				interval: 100
			},
			data: [{
				type: 'line',
				name: 'Fluid Pound',
				markerType: 'none',
				dataPoints: Array.from({ length: 5000 }, () => ({ y: this.chance.floating({ min: 0, max: 400 }) })),
			}]
		}) as Canvas.Chart;

		chart.render();
	}

	loadDynacardGrid() {
		const options = {
			animationEnabled: true,
			zoomEnabled: false,
			title: {
				text: ''
			},
			toolTip: {
				enabled: false,
			},
			axisX: {
				gridThickness: 0,
				tickLength: 0,
				lineThickness: 0,
				labelFormatter: () => '',
				minimum: 0
			},
			axisY: {
				gridThickness: 0,
				tickLength: 0,
				lineThickness: 0,
				labelFormatter: () => '',
				minimum: 0
			},
			data: []
		};



		this.dataService.getDynaData(100).subscribe((data: any[]) => {
			this.dynacards = data;
			data.forEach((dynadata, index) => {
				this.gridItems.nativeElement.insertAdjacentHTML('beforeend',
					`<div style="height: 200px; flex-basis: 20%;">
						<div id="dynaContainer${index}" style="height: 200px;"></div>
					</div>`);

				options.data = [{
					type: 'line',
					lineThickness: 3,
					markerSize: 0,
					lineColor: '#212322',
					dataPoints: dynadata.map(([x, y]) => ({ x, y }))
				}];

				const chart = new CanvasJS.Chart(`dynaContainer${index}`, options);
				chart.render();
			});
		});

		// this.dynacards = x;
	}
}
