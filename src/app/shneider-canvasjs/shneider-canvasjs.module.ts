import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShneiderCanvasjsComponent } from './shneider-canvasjs.component';
import { ShneiderCanvasjsRoutingModule } from './shneider-canvasjs-routing.module';

@NgModule({
  imports: [
	CommonModule,
	ShneiderCanvasjsRoutingModule
  ],
  declarations: [ShneiderCanvasjsComponent]
})
export class ShneiderCanvasjsModule { }
