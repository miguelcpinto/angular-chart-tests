import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShneiderCanvasjsComponent } from './shneider-canvasjs.component';

const routes: Routes = [
	{
		path: '',
		component: ShneiderCanvasjsComponent
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
	providers: []
})
export class ShneiderCanvasjsRoutingModule { }
