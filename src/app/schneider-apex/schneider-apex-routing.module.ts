import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SchneiderApexComponent } from './schneider-apex.component';

const routes: Routes = [
	{
		path: '',
		component: SchneiderApexComponent
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
	providers: []
})
export class SchneiderApexRoutingModule { }
