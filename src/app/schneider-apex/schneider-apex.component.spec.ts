import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchneiderApexComponent } from './schneider-apex.component';

describe('SchneiderApexComponent', () => {
  let component: SchneiderApexComponent;
  let fixture: ComponentFixture<SchneiderApexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchneiderApexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchneiderApexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
