import { Component, OnInit } from '@angular/core';
import { Chance } from 'chance';
import { ApexChart } from 'ng-apexcharts';
import moment from 'moment';

@Component({
	selector: 'app-schneider-apex',
	templateUrl: './schneider-apex.component.html',
	styleUrls: ['./schneider-apex.component.sass']
})
export class SchneiderApexComponent implements OnInit {
	chance: Chance.Chance;

	chart0;
	chart1: any;
	chart2: any;

	// tslint:disable-next-line: max-line-length
	dyna1 = [[5, 80], [3, 150], [12, 350], [62, 330], [75, 305], [82, 290], [85, 90], [75, 50], [35, 65], [28, 50], [18, 70], [9, 45], [5, 80]];
	// tslint:disable-next-line: max-line-length
	dyna2 = [[2, 75], [3, 140], [12, 340], [62, 320], [75, 290], [82, 285], [85, 85], [75, 45], [35, 60], [28, 45], [18, 65], [8, 40], [2, 75]];
	// tslint:disable-next-line: max-line-length
	dyna3 = [[2, 70], [4, 130], [11, 330], [62, 310], [76, 280], [82, 275], [84, 75], [62, 35], [35, 50], [29, 40], [18, 65], [7, 35], [2, 70]];

	constructor() {
		this.chance = new Chance();
	}

	ngOnInit() {
		this.chart1 = {
			chart: {
				type: 'line',
				height: 400,
				animations: {
					enabled: true
				},
				toolbar: {
					show: false,
				},
				fontFamily: 'Lucida Grande'
			},
			series: [
				{
					name: 'Fluid Pound',
					data: Array.from({ length: 24 }, (_, index) => ([
						moment().startOf('day').add(index, 'hour'),
						this.chance.floating({ min: 0, max: 400 })
					])),
				},
				{
					name: 'Gas Lock',
					data: Array.from({ length: 24 }, (_, index) => ([
						moment().startOf('day').add(index, 'hour'),
						this.chance.floating({ min: 0, max: 10 })
					])),
				},
				{
					name: 'Gas Pressure',
					data: Array.from({ length: 24 }, (_, index) => ([
						moment().startOf('day').add(index, 'hour'),
						this.chance.floating({ min: 0, max: 350 })
					])),
				}
			],
			xaxis: {
				type: 'datetime',
				tootltip: {
					enabled: false
				},
				labels: {
					datetimeFormatter: {
						hour: 'h:mm tt'
					},
					style: {
						colors: ['#979797'],
						fontSize: '10px',
						fontFamily: 'Lucida Grande'
					},
					offsetX: 25,
					offsetY: -5,
				},
				axisTicks: {
					show: false
				},
				axisBorder: {
					show: true,
					color: '#e9ebf1',
				},
			},
			yaxis: {
				tootltip: {
					enabled: false
				},
				labels: {
					datetimeFormatter: {
						hour: 'h:mm tt'
					},
					style: {
						colors: ['#979797'],
						fontSize: '10px',
						fontFamily: 'Lucida Grande'
					},

				},
				axisTicks: {
					show: false
				},
				decimalsInFloat: 0
			},
			grid: {
				borderColor: '#e9ebf1',
				strokeDashArray: 1,
				xaxis: {
					lines: {
						show: true
					}
				},
				yaxis: {
					lines: {
						show: true,
					}
				},
				padding: {
					top: 0,
					right: 30,
					bottom: 0,
					left: 30
				},
			},
			legend: {
				show: false
			},
			stroke: {
				width: 2
			},
			states: {
				hover: {
					filter: {
						type: 'lighten',
						value: 1,
					}
				},
			},
			tooltip: {
				followCursor: true,
				onDatasetHover: {
					highlightDataSeries: true,
				},
			}
		};

		this.chart2 = {
			chart: {
				type: 'line',
				height: 350,
				toolbar: {
					show: false,
				},
				fontFamily: 'Lucida Grande'
			},
			series: [
				{
					data: this.dyna1
				},
				{
					data: this.dyna2
				},
				{
					data: this.dyna3
				}
			],
			xaxis: {
				crosshairs: {
					show: true
				},
				tooltip: {
					enabled: true,
					stroke: {
						color: '#b6b6b6',
						width: 2,
						dashArray: 0,
					}
				},
				labels: {
					style: {
						colors: ['#979797'],
						fontSize: '10px',
						fontFamily: 'Lucida Grande'
					},
					offsetX: 25,
					offsetY: -5,
				},
			},
			yaxis: {
				crosshairs: {
					show: true,
					stroke: {
						color: '#b6b6b6',
						width: 2,
						dashArray: 0,
					}
				},
				labels: {
					style: {
						colors: ['#979797'],
						fontSize: '10px',
						fontFamily: 'Lucida Grande'
					},
				},
				tooltip: {
					enabled: true
				}
			},
			grid: {
				borderColor: '#e9ebf1',
				strokeDashArray: 1,
				xaxis: {
					lines: {
						show: true
					},
				},
				yaxis: {
					lines: {
						show: true,
					},
				},
				padding: {
					top: 0,
					right: 30,
					bottom: 0,
					left: 30
				},
			},
			legend: {
				show: false
			},
			stroke: {
				show: true,
				lineCap: 'butt',
				width: 2
			},
			states: {},
			tooltip: {
				// enabled: false,
				// followCursor: true,
				// inverseOrder: true,
				x: {
					show: false
				},
				shared: true,
				fixed: {
					enabled: true,
					position: 'topLeft', // topRight, topLeft, bottomRight, bottomLeft
					offsetY: 30,
					offsetX: 60
				},
				marker: {
					show: false,
				},
			},
			markers: {
				size: 0,
				hover: {
					size: 0
				}
			}
			// tooltip: {
			// 	shared: false,
			// 	followCursor: true,
			// 	// intersect: true,
			// 	onDatasetHover: {
			// 		highlightDataSeries: true,
			// 	},
			// }
		};

		this.chart0 = {
			chart: {
				type: 'line',
				height: 400,
				animations: {
					enabled: true
				},
				toolbar: {
					show: false,
				},
				fontFamily: 'Lucida Grande'
			},
			series: [
				{
					name: 'Fluid Pound',
					data: Array.from({ length: 5000 }, (_, index) => ([
						moment().startOf('day').add(index, 'minutes'),
						this.chance.floating({ min: 0, max: 400 })
					])),
				}
			],
			xaxis: {
				type: 'datetime',
				tootltip: {
					enabled: false
				},
				labels: {
					datetimeFormatter: {
						hour: 'h:mm tt'
					},
					style: {
						colors: ['#979797'],
						fontSize: '10px',
						fontFamily: 'Lucida Grande'
					},
					offsetX: 25,
					offsetY: -5,
				},
				axisTicks: {
					show: false
				},
				axisBorder: {
					show: true,
					color: '#e9ebf1',
				},
			},
			yaxis: {
				tootltip: {
					enabled: false
				},
				labels: {
					datetimeFormatter: {
						hour: 'h:mm tt'
					},
					style: {
						colors: ['#979797'],
						fontSize: '10px',
						fontFamily: 'Lucida Grande'
					},

				},
				axisTicks: {
					show: false
				},
				decimalsInFloat: 0
			},
			grid: {
				borderColor: '#e9ebf1',
				strokeDashArray: 1,
				xaxis: {
					lines: {
						show: true
					}
				},
				yaxis: {
					lines: {
						show: true,
					}
				},
				padding: {
					top: 0,
					right: 30,
					bottom: 0,
					left: 30
				},
			},
			legend: {
				show: false
			},
			stroke: {
				width: 2
			},
			states: {
				hover: {
					filter: {
						type: 'lighten',
						value: 1,
					}
				},
			},
			tooltip: {
				followCursor: true,
				onDatasetHover: {
					highlightDataSeries: true,
				},
			}
		};
	}
}
