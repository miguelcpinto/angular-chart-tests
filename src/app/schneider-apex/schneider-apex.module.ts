import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SchneiderApexComponent } from './schneider-apex.component';
import { SchneiderApexRoutingModule } from './schneider-apex-routing.module';
import { NgApexchartsModule } from 'ng-apexcharts';

@NgModule({
	declarations: [SchneiderApexComponent],
	imports: [
		CommonModule,
		SchneiderApexRoutingModule,
		NgApexchartsModule
	]
})
export class SchneiderApexModule { }
