import * as express from 'express';
import * as http from 'http';
import * as WebSocket from 'ws';
import * as cors from 'cors';
import { Chance } from 'chance';
import { timer } from 'rxjs';
import { readFile, read } from 'fs';
import * as Papa from 'papaparse';

const DEFAULT_LIST_SIZE = 25;

const app = express();

app.use(cors());

const server = http.createServer(app);

const wss = new WebSocket.Server({ server });

const chance = new Chance();

// emits data every 500ms
const sourceTimer = timer(1000, 1000);

let csvData: any[] = [];

readFile('server/pump1_20181124.csv', 'utf8', (err, data) => {
    Papa.parse(data, {
        header: true,
        complete: ({ data }) => {
            csvData = data;
        }
    });
});

// create a websocket connection
wss.on('connection', (ws: WebSocket) => {
    sourceTimer.subscribe(() => {
        const randomNumber = chance.floating({ min: 0, max: 100 });

        ws.send(randomNumber);
    });
});

// create a request to handle data list requests
app.get('/data/:number', (req, res) => {
    const length = Number(req.params.number) || DEFAULT_LIST_SIZE;
    const data = Array.from({ length }, () => chance.floating({ min: 0, max: 100 }));

    res.send(data);
});

// create a request to handle data list requests
app.get('/dyna/:number', (req, res) => {
    const length = Number(req.params.number) || 10;
    const data = csvData.slice(0, length);
    const joinAxes = req.query.join === 'true';

    const dataToSend = data.map(dyna => {
        dyna.position_rescaled = eval(dyna.position_rescaled);
        dyna.load_rescaled = eval(dyna.load_rescaled);

        return dyna.position_rescaled.map((x: any, index: any) => ([x, dyna.load_rescaled[index]]))
    });

    setTimeout(() => {
        res.send(dataToSend);
    }, 2000);
});

// start our server
server.listen(process.env.PORT || 8999, () => {
    const serverInfo: any = server.address();
    console.log(`Server started on port ${serverInfo.port}`);
});
